package com.dtl.demo;

public interface Command<I, O> {
    O execute(I input);
}