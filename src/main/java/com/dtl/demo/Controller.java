package com.dtl.demo;

import com.dtl.demo.service.ResourceManager;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

@Path("properties")
public class Controller {

    @Inject
    private ResourceManager manager;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSystemInfo() {
        return Response.ok(manager.execute(null)).build();
    }
}
