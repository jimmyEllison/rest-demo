package com.dtl.demo.service;

import com.dtl.demo.Command;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.json.Json;
import jakarta.json.JsonObject;
import jakarta.json.JsonObjectBuilder;

@ApplicationScoped
public class ResourceManager implements Command<Void, JsonObject> {

    private static final JsonObjectBuilder builder = Json.createObjectBuilder();

    private static JsonObject getSystemInfo() {
        for (var entry : System.getProperties().entrySet()) {
            builder.add((String) entry.getKey(), ((String) entry.getValue()).strip());
        }
        return builder.build();
    }

    @Override
    public JsonObject execute(Void input) {
        return getSystemInfo();
    }
}
